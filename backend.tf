terraform {
  backend "s3" {
    bucket = "aws-marcos"
    key    = "eks-tf/terraform.tfstate"
    region = "us-east-2"
  }
}
